const Carts = require('../models').cart;
const Users = require('../models').user;
const Product = require('../models').product;
const jwt = require('jsonwebtoken')

Product.hasMany(Carts);
Carts.belongsTo(Product);

Users.hasMany(Carts);
Carts.belongsTo(Users);

//check if product already exist at cart
const checkCart = async(product_id) => {
  const result = await Carts.findAll({
    where: {
      productId: product_id
    }
  })
  if (result.length != 0) {
    const qtty = result[0].quantity; 
    return qtty
  }
}

//check product stock
const checkStock = async(product_id, quantity) => {
  const result = await Product.findAll({
    where: {
      id: product_id
    }
  });
  if (result) {
    const stockAvail = result[0].stock - quantity;
    if (stockAvail <= 0) return false; 
  }
}

//add product to cart
const add = async(req, res) => {
  const { product_id, quantity } = req.body;
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  const userEmail = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
    return decoded.email;
  });
  try {
    const stockAvail = checkStock(product_id, quantity);
    if ( !stockAvail ) return res.send({ msg: "Stock is not available" });
    const check = await checkCart(product_id);
    if (check) {
      const productQtty = await Carts.findAll({
        where: { 
          productId: product_id
        }
      });
      if (productQtty) {
        const lastQtty = parseInt(productQtty[0].quantity)
        const totalqtty = lastQtty + parseInt(quantity)
        await Carts.update({quantity: totalqtty}, {
          where: { productId: product_id }
        });
        return res.status(200).send({ msg: 'Quantity item added' })
      }
    }
    
    const userData = await Users.findAll({
      where: {
        email: userEmail
      }
    });

    const id = userData[0].id;
    await Carts.create({
      quantity: quantity,
      productId: product_id,
      credentialId: id,
    });
    res.status(201).json({ msg: 'Item successfully added!' });
  } catch (err) {
    res.json({ error: err.name });
  };
};

const remove =  async(req, res) => {
  const { id } = req.body;
  try {
    await Carts.destroy({
      where: {
        id: id
      }
    });
    res.json({msg: 'Product removed from Cart!'});
  } catch (err) {
    res.json({ error: err.name });
  };
};

const update = async(req, res) => {
  try {
    const { id, quantity } = req.body;
    Carts.update({ quantity: quantity }, {
      where: {
        id: id
      }
    })
    res.json({ msg: "Product quantity succesfully updated!" });
  } catch (err) {
    res.json({ error: err.name });
  };
};

//show all products in cart
const showAll = async(req, res) => {
  try {
    const result = await Carts.findAll({
      include: { model: Product, attributes: ['name'],required:false },
      attributes: ['id', 'quantity']
    });
    res.send(result);
  } catch (err) {
    res.send({ error: err.name });
  };
};

//show single product
const showProduct = async(req, res) => {
  try {
    const result = await Carts.findOne({
      where: {
        id: req.params.id
      },
      include: { model: Product, attributes: ['name'],required:false },
      attributes: ['id', 'quantity']
    })
    res.json(result)
  }
  catch (err) {
    res.json({ msg: err.name })
  }
};

module.exports = {
  add, showAll, update, remove, showProduct
}