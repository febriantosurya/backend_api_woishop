const Users = require('../models').user;
const jwt = require('jsonwebtoken');

const getUser = async (req, res) => {
  try {
    const users = await Users.findAll();
    res.json(users);
  } catch(err) {
    res.json({ error: err.name })
  };
};

const register = async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await Users.findOne({
      where: {
        email: email
      }
    });
    if (user) return res.status(400).json({ msg: "Email already registered!" });
    await Users.create({
      email: email,
      password: password
    });
    res.json({ msg: 'Register Success!' });
  } catch (err) {
    res.json({ error: err.name });
  };
};

const login = async(req, res) => {
  try {
    const user = await Users.findAll({
      where: {
        email: req.body.email
      }
    });
    if (req.body.password != user[0].password) return res.status(400).json({msg: "Wrong Password!"});
    const userId = user[0].id;
    const email = user[0].email;
    const password = user[0].password;
    const accessToken = jwt.sign({ userId, email, password }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '360s' });
    res.json({ accessToken });
  } catch(error) {
    res.status(404).json({ msg: "Email not found!" });
  };
};

module.exports = { getUser, register, login };