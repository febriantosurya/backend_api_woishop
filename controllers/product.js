const Category = require('../models').category;
const Product = require('../models').product;

Category.hasMany(Product);
Product.belongsTo(Category);

const getCategory = async(req, res) => {
    Category.findAll().then(result => {
      res.send(result);
    }).catch((err) => {
      res.json({ error: err.name })
    });
};

const getAllProduct = (req, res) => {
    Product.findAll().then(result => {
      res.send((result));
    }).catch((err) => {
      res.json({ error: err.name })
    });
};

const getProductByCategory = (req, res) => {
    Product.findAll({
      where: {categoryId:req.params.id},
      include: { model: Category, required: false },
    }).then(result => {
        res.send(result);
    }).catch((err) => {
        res.json({ error: err.name})
    });
};

const getDetailProduct = (req, res) => {
    Product.findByPk(req.params.id).then(result => {
        res.send(result);
    }).catch((err) => {
        res.json({ error: err.name })
    });
}
module.exports = {
  getCategory, getAllProduct, getDetailProduct, getProductByCategory
}