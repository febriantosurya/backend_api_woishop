const express = require('express');
const dotenv = require('dotenv');
const router = require('./routes/index');
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
dotenv.config();

const app = express();
const PORT = 3000;

const options = {
  definition : {
    openapi: "3.0.0",
    info: {
      title: "WoiShop API",
      version: "1.0.0",
      description: "REST API for WoiShop"
    },
    servers: [{
      url: "http://localhost:3000"
    }]
  },
  apis: ["./routes/*.js"]
};

const specs = swaggerJsDoc(options);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(specs));
app.use(express.json());
app.use(router);

app.listen(PORT, () => {
  console.log(`Running on PORT : ${PORT}`);
});