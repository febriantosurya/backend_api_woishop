const express = require('express');
const product = require('../controllers/product');
const { verif } = require('../middleware/verifToken');

const router = express.Router();

/**
 * @swagger
 * components:
 *  securitySchemes:
 *    BearerAuth:
 *      type: http
 *      scheme: bearer
 *  schemas:
 *    Category:
 *      type: object
 *      required:
 */

/**
 * @swagger
 * tags:
 *  name: Product
 *  description: API to show category and product
 */

/**
 * @swagger
 * /product/category:
 *  get:
 *    security:
 *      - BearerAuth: []
 *    summary: show all categories
 *    tags: [Product]
 *    description: this api to show categories
 *    responses:
 *      200:
 *        description: success
 */

/**
 * @swagger
 * /product/all:
 *  get:
 *    security:
 *      - BearerAuth: []
 *    summary: show all products
 *    tags: [Product]
 *    description: This api to show products in the store
 *    responses:
 *      200:
 *        description: success
 */

/**
 * @swagger
 * /product/category/{id}:
 *  get:
 *    security:
 *      - BearerAuth: []
 *    summary: show all products
 *    tags: [Product]
 *    description: This api to show products in a category
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of product required
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: success
 */

/**
 * @swagger
 * /product/detail/{id}:
 *  get:
 *    security:
 *      - BearerAuth: []
 *    summary: show all products
 *    tags: [Product]
 *    description: This api to show a product by detail
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of product required
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: success
 */

router.get('/category', verif, product.getCategory);
router.get('/all', verif, product.getAllProduct);
router.get('/category/:id', verif, product.getProductByCategory);
router.get('/detail/:id', verif, product.getDetailProduct);

module.exports = router;