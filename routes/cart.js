const express = require('express');
const cart = require('../controllers/cart');
const { verif } = require('../middleware/verifToken');

const router = express.Router();

/**
 * @swagger
 * components:
 *  securitySchemes:
 *    BearerAuth:
 *      type: http
 *      scheme: bearer
 *  schemas:
 *    AddCart:
 *      type: object
 *      required:
 *        - product_id
 *        - quantity
 *      properties:
 *        product_id:
 *          type: integer
 *          description: product id
 *        quantity:
 *          type: integer
 *          description: quantity product item
 *    UpdateCart:
 *      type: object
 *      required:
 *        - id
 *        - quantity
 *      properties:
 *        id:
 *          type: integer
 *          description: id product from cart
 *        quantity:
 *          type: integer
 *          description: quantity product item
 *    RemoveCart:
 *      type: object
 *      required:
 *        - id
 *      properties:
 *        id:
 *          type: integer
 *          description: id product from cart
 */

/**
 * @swagger
 * tags:
 *  name: Cart
 *  description: API for CRUD on cart
 */

/**
 * @swagger
 * /cart/add:
 *  post:
 *    summary: Add product
 *    tags: [Cart]
 *    description: this api to add product to cart
 *    security:
 *      - BearerAuth : []
 *    requestBody:
 *      required:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#components/schemas/AddCart'
 *    responses:
 *      200:
 *        description: Quantity item added
 *      201:
 *        description: Item added to Cart
 */

/**
 * @swagger
 * /cart/update:
 *  post:
 *    summary: Update product
 *    tags: [Cart]
 *    description: this api to update the quantity product in cart
 *    security:
 *      - BearerAuth : []
 *    requestBody:
 *      required:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#components/schemas/UpdateCart'
 *    responses:
 *      200:
 *        description: Product successfully updated
 */

/**
 * @swagger
 * /cart/remove:
 *  post:
 *    summary: Remove product
 *    tags: [Cart]
 *    description: this api to remove product from the cart
 *    security:
 *      - BearerAuth : []
 *    requestBody:
 *      required:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#components/schemas/RemoveCart'
 *    responses:
 *      200:
 *        description: Product successfully removed
 */

/**
 * @swagger
 * /cart/show:
 *  get:
 *    security:
 *      - BearerAuth: []
 *    summary: show cart
 *    tags: [Cart]
 *    description: this api to show all products in cart
 *    responses:
 *      200:
 *        description: success
 */

/**
 * @swagger
 * /cart/{id}:
 *  get:
 *    security:
 *      - BearerAuth: []
 *    summary: show a product
 *    tags: [Cart]
 *    description: This api to show a product in cart
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: Numeric ID of product from cart is required
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: success
 */

router.post('/add', cart.add);
router.post('/update', cart.update);
router.post('/remove', cart.remove);
router.get('/show', cart.showAll);
router.get('/:id', cart.showProduct);

module.exports = router;
