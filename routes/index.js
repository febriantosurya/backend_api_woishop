const express = require('express');
const getUser = require('./user');
const getProduct = require('./product');
const getCart = require('./cart');
const { verif } = require('../middleware/verifToken');

const router = express.Router();

router.use('/account', getUser);
router.use('/product', getProduct);
router.use('/cart', verif, getCart);

module.exports = router;