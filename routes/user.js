const express = require('express');
const user = require('../controllers/user');

const router = express.Router();

/**
 * @swagger
 * components:
 *  schemas:
 *    User:
 *      type: object
 *      required:
 *        - email
 *        - password
 *      properties:
 *        email:
 *          type: string
 *          description: user email
 *        password:
 *          type: string
 *          description: user password
 */

/**
 * @swagger
 * tags:
 *  name: User
 *  description: API for login and register
 */

/**
 * @swagger
 * /account/register:
 *  post:
 *    summary: To register an user
 *    tags: [User]
 *    description: this api to input data user to database if email unregistered
 *    requestBody:
 *      required:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#components/schemas/User'
 *    responses:
 *      200:
 *        description: user added
 *      400:
 *        description: user already registered!
 */

/**
 * @swagger
 * /account/login:
 *  post:
 *    summary: To login registered user and generate token
 *    tags: [User]
 *    requestBody:
 *      required:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#components/schemas/User'
 *    responses:
 *      200:
 *        description: login success!
 *      404:
 *        description: email not found!
 */
router.post('/register', user.register);
router.post('/login', user.login);

module.exports = router;