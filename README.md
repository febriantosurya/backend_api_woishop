# WOISHOP WEB APP REST API
In this project, i create REST API for WoiShop Web App.
[![made-with-javascript](https://img.shields.io/badge/Made%20with-JavaScript-1f425f.svg)](https://www.javascript.com)

## How to use
Please visit link below to see REST API documentation
http://45.76.184.44:3005/api-docs

## Features
Several APIs in this project, there are:
- Register
- Login
- List of Product
- Cart

## Tech
Some package that used in this project:
- [Express](https://www.npmjs.com/package/express) is used for routing.
- [JsonWebToken](https://www.npmjs.com/package/jwt) is used for generate token for authorization
- [Sequelize](https://www.npmjs.com/package/sequelize) is used for configure and connect to PostgreSQL
- [dotnev](https://www.npmjs.com/package/dotenv) is used for loads environment variables from a . env file into the process

## Requirements
- express 4.18.1
- dotenv 16.0.2
- jswonwebtoken 8.5.1
- sequelize 6.21.6